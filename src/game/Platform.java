package game;

import components.objects.*;
import components.characters.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.JPanel;

import components.objects.Object;
import utils.Res;
import utils.Utils;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    private static final int MARIO_FREQUENCY = 25;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final int TURTLE_FREQUENCY = 45;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;
    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;
    private static final int GAME_OVER_X_POSITION = 310;
    private static final int GAME_OVER_Y_POSITION = -40;
    private static final int TRY_AGAIN_X_POSITION = 55;
    private static final int TRY_AGAIN_Y_POSITION = 240;
    private static final int SCORE_X_POSITION = 600;
    private static final int SCORE_Y_POSITION = 30;
    private Image imageBackground1;
    private Image imageBackground2;
    private Image imageGameOver;
    private Image imageTryAgain;
    private Image castle;
    private Image start;
    private Score score;

    private int background1XPosition;
    private int background2XPosition;
    private int movement;
    private int xPosition;
    private int floorOffsetY;
    private int heightLimit;

    private Mario mario;
    private Enemy mushroom;
    private Enemy turtle;

    private Image imageFlag;
    private Image imageCastle;

    private CharacterFactory characterFactory;
    private ObjectFactory objectFactory;
    private ArrayList<Object> objects;
    private ArrayList<Money> money;

    public Platform() {
        super();
        this.background1XPosition = -50;
        this.background2XPosition = 750;
        this.movement = 0;
        this.xPosition = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.imageBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imageBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imageGameOver = Utils.getImage(Res.IMG_GAME_OVER);
        this.imageTryAgain = Utils.getImage(Res.IMG_TRY_AGAIN);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);
        this.score = new ScoreImpl();

        this.createComponent();

        this.imageCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imageFlag = Utils.getImage(Res.IMG_FLAG);

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
    }

    public Mario getMario(){
        return mario;
    }

    private void createComponent(){
        characterFactory = new CharacterFactoryImpl();
        this.createMario();
        this.createEnemies();

        objectFactory = new ObjectFactoryImpl();
        objects = new ArrayList<>();
        this.createTunnels();
        this.createBlocks();

        money = new ArrayList<>();
        this.createMoney();
    }

    private void createMario(){
     this.mario =  (Mario) characterFactory.createMario(300, 245);
    }

    private void createEnemies(){
        this.mushroom =  characterFactory.createMushroom(800, 263);
        this.turtle =  characterFactory.createTurtle(950, 243);
    }

    private void createTunnels(){
        this.objects.add(objectFactory.createTunnel(600, 230));
        this.objects.add(objectFactory.createTunnel(1000, 230));
        this.objects.add(objectFactory.createTunnel(1900, 230));
        this.objects.add(objectFactory.createTunnel(2500, 230));
        this.objects.add(objectFactory.createTunnel(3000, 230));
        this.objects.add(objectFactory.createTunnel(3800, 230));
        this.objects.add(objectFactory.createTunnel(4500, 230));
    }

    private void createBlocks(){
        this.objects.add(objectFactory.createBlock(400, 180));
        this.objects.add(objectFactory.createBlock(1200, 180));
        this.objects.add(objectFactory.createBlock(1270, 170));
        this.objects.add(objectFactory.createBlock(1340, 160));
        this.objects.add(objectFactory.createBlock(2000, 180));
        this.objects.add(objectFactory.createBlock(2600, 160));
        this.objects.add(objectFactory.createBlock(2650, 180));
        this.objects.add(objectFactory.createBlock(3500, 160));
        this.objects.add(objectFactory.createBlock(3550, 140));
        this.objects.add(objectFactory.createBlock(4000, 170));
        this.objects.add(objectFactory.createBlock(4200, 200));
        this.objects.add(objectFactory.createBlock(4300, 210));
    }

    private void createMoney(){
        this.money.add((Money) objectFactory.createMoney(402, 145));
        this.money.add((Money) objectFactory.createMoney(1202, 140));
        this.money.add((Money) objectFactory.createMoney(1272, 95));
        this.money.add((Money) objectFactory.createMoney(1342, 40));
        this.money.add((Money) objectFactory.createMoney(1650, 145));
        this.money.add((Money) objectFactory.createMoney(2650, 145));
        this.money.add((Money) objectFactory.createMoney(3000, 135));
        this.money.add((Money) objectFactory.createMoney(3400, 125));
        this.money.add((Money) objectFactory.createMoney(4200, 145));
        this.money.add((Money) objectFactory.createMoney(4600, 40));
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMovement() {
        return movement;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setBackground2XPosition(int background2XPosition) {
        this.background2XPosition = background2XPosition;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public void setMovement(int movement) {
        this.movement = movement;
    }

    public void setBackground1XPosition(int x) {
        this.background1XPosition = x;
    }

    public void updateBackgroundOnMovement() {
        if (this.xPosition >= 0 && this.xPosition <= 4600) {
            this.xPosition = this.xPosition + this.movement;
            // Moving the screen to give the impression that Mario is walking
            this.background1XPosition = this.background1XPosition - this.movement;
            this.background2XPosition = this.background2XPosition - this.movement;
        }
        this.flipBackground();
    }

    private void flipBackground(){
        // Flipping between background1 and background2
        if (this.background1XPosition == -800) {
            this.background1XPosition = 800;
        } else if (this.background2XPosition == -800) {
            this.background2XPosition = 800;
        } else if (this.background1XPosition == 800) {
            this.background1XPosition = -800;
        } else if (this.background2XPosition == 800) {
            this.background2XPosition = -800;
        }
    }

    public void paintComponent(Graphics graphic) {
        super.paintComponent(graphic);
        Graphics graphic2D = (Graphics2D) graphic;

        this.contactWithObject();
        this.checkContactWithMoney();
        this.contactWithCharacter();

        this.moveFixedComponents();

        this.drawBackgrounds(graphic2D);
        this.drawCastle(graphic2D);
        this.drawStart(graphic2D);
        this.drawObjects(graphic2D);
        this.drawMoney(graphic2D);
        this.drawImageFlag(graphic2D);
        this.drawImageCastle(graphic2D);
        this.drawScore(graphic2D);

        this.drawMario(graphic2D);
        this.drawMushroom(graphic2D);
        this.drawTurtle(graphic2D);

        this.checkMarioDeath(graphic2D);
    }

    private void contactWithObject(){
        for (int i = 0; i < objects.size(); i++) {
            if (this.mario.isNearby(this.objects.get(i)))
                this.mario.contactWithObject(this.objects.get(i));

            if (this.mushroom.isNearby(this.objects.get(i)))
                this.mushroom.contact(this.objects.get(i));

            if (this.turtle.isNearby(this.objects.get(i)))
                this.turtle.contact(this.objects.get(i));
        }
    }

    private void checkContactWithMoney(){
        for (int i = 0; i < money.size(); i++) {
            if (this.mario.contactMoney(this.money.get(i))) {
                Audio.playSound(Res.AUDIO_MONEY);
                this.money.remove(i);
                this.updateScore(new MoneyScoreStrategy());
            }
        }
    }

    private void contactWithCharacter(){
        if (this.mushroom.isNearby(turtle)) {
            this.mushroom.contact(turtle);
        }
        if (this.turtle.isNearby(mushroom)) {
            this.turtle.contact(mushroom);
        }
        if (this.mario.isNearby(mushroom)) {
            this.mario.contactWithCharacter(mushroom);
        }
        if (this.mario.isNearby(turtle)) {
            this.mario.contactWithCharacter(turtle);
        }
    }

    private void moveFixedComponents(){
        this.updateBackgroundOnMovement();
        if (this.xPosition >= 0 && this.xPosition <= 4600) {
            this.moveObjects();
            this.moveMoney();
            this.moveEnemies();
        }
    }

    private void moveObjects(){
        for (int i = 0; i < objects.size(); i++) {
            objects.get(i).moveScene();
        }
    }

    private void moveMoney(){
        for (int i = 0; i < money.size(); i++) {
            this.money.get(i).moveScene();
        }
    }

    private void moveEnemies(){
        this.mushroom.moveScene();
        this.turtle.moveScene();
    }

    private void drawBackgrounds(Graphics graphic){
        graphic.drawImage(this.imageBackground1, this.background1XPosition, 0, null);
        graphic.drawImage(this.imageBackground2, this.background2XPosition, 0, null);
    }

    private void drawCastle(Graphics graphic){
        graphic.drawImage(this.castle, 10 - this.xPosition, 95, null);
    }

    private void drawStart(Graphics graphic){
        graphic.drawImage(this.start, 220 - this.xPosition, 234, null);
    }

    private void drawObjects(Graphics graphic){
        for (int i = 0; i < objects.size(); i++) {
            graphic.drawImage(this.objects.get(i).getImageObject(), this.objects.get(i).getX(),
                    this.objects.get(i).getY(), null);
        }
    }

    private void drawMoney(Graphics graphic){
        for (int i = 0; i < money.size(); i++) {
            graphic.drawImage(this.money.get(i).imageOnMovement(), this.money.get(i).getX(),
                    this.money.get(i).getY(), null);
        }
    }

    private void drawImageFlag(Graphics graphic){
        graphic.drawImage(this.imageFlag, FLAG_X_POS - this.xPosition, FLAG_Y_POS, null);
    }

    private void drawImageCastle(Graphics graphic){
        graphic.drawImage(this.imageCastle, CASTLE_X_POS - this.xPosition, CASTLE_Y_POS, null);
    }

    private void drawScore(Graphics graphic){
        graphic.drawString("SCORE: " + this.score.getScore(), SCORE_X_POSITION, SCORE_Y_POSITION);
    }

    private void drawMario(Graphics graphic){
        if (this.mario.isJumping()) {
            graphic.drawImage(this.mario.doJump(), this.mario.getX(), this.mario.getY(), null);
        } else {
            graphic.drawImage(this.mario.walk(Res.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY), this.mario.getX(), this.mario.getY(), null);
        }
    }

    private void drawMushroom(Graphics graphic){
        if (this.mushroom.isAlive()) {
            graphic.drawImage(this.mushroom.walk(Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY), this.mushroom.getX(), this.mushroom.getY(), null);
        } else {
            graphic.drawImage(this.mushroom.deadImage(), this.mushroom.getX(), this.mushroom.getY() + MUSHROOM_DEAD_OFFSET_Y, null);
        }
    }

    private void drawTurtle(Graphics graphic){
        if (this.turtle.isAlive()) {
            graphic.drawImage(this.turtle.walk(Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY), this.turtle.getX(), this.turtle.getY(), null);
        } else {
            graphic.drawImage(this.turtle.deadImage(), this.turtle.getX(), this.turtle.getY() + TURTLE_DEAD_OFFSET_Y, null);
        }
    }

    private void checkMarioDeath(Graphics graphic){
        if( !this.mario.isAlive()) {
            Main.refresh.stopGame();
            this.score.reset();
            Audio.playSound(Res.AUDIO_GAME_OVER);
            graphic.drawImage(this.imageGameOver, this.mario.getX() - GAME_OVER_X_POSITION, GAME_OVER_Y_POSITION, null);
            graphic.drawImage(this.imageTryAgain, this.mario.getX() - TRY_AGAIN_X_POSITION, TRY_AGAIN_Y_POSITION, null);
        }
    }

    public void updateScore(ScoreStrategy strategy){
        this.score.setScoreStrategy(strategy);
        this.score.increment();
    }
}
