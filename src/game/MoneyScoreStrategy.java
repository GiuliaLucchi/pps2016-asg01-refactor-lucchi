package game;

/**
 * Created by Giulia on 16/03/2017.
 */
public class MoneyScoreStrategy implements ScoreStrategy {

    @Override
    public int doIncrement(int score) {
        return score + 1;
    }
}
