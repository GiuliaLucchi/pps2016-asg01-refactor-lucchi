package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        if (Main.scene.getMario().isAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (Main.scene.getxPosition() == -1) {
                    Main.scene.setxPosition(0);
                    Main.scene.setBackground1XPosition(-50);
                    Main.scene.setBackground2XPosition(750);
                }
                Main.scene.getMario().setMoving(true);
                Main.scene.getMario().setToRight(true);
                Main.scene.setMovement(1); // si muove verso sinistra
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (Main.scene.getxPosition() == 4601) {
                    Main.scene.setxPosition(4600);
                    Main.scene.setBackground1XPosition(-50);
                    Main.scene.setBackground2XPosition(750);
                }

                Main.scene.getMario().setMoving(true);
                Main.scene.getMario().setToRight(false);
                Main.scene.setMovement(-1); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                Main.scene.getMario().setJumping(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }else{
            if (e.getKeyCode() == KeyEvent.VK_Z) {
                Main.window.setVisible(false);
                String[] args = {};
                Main.main(args);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.scene.getMario().setMoving(false);
        Main.scene.setMovement(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
