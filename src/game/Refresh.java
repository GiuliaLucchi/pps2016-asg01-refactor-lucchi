package game;

public class Refresh implements Runnable {

    private final int PAUSE = 3;
    private boolean isPlaying = true;

    public void run() {
        while (isPlaying) {
            Main.scene.repaint();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

    public void stopGame(){
        isPlaying = false;
    }

} 
