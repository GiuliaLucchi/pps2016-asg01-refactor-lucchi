package game;

/**
 * Created by Giulia on 16/03/2017.
 */
public interface ScoreStrategy {

    int doIncrement(int score);

}
