package components.characters;
import components.Component;

import java.awt.Image;

public interface Character extends Component{

	int getCounter();

	boolean isAlive();

	boolean isMoving();

	boolean isToRight();

	void setAlive(boolean alive);

	void setMoving(boolean moving);

	void setToRight(boolean toRight);

	void setCounter(int counter);

	Image walk(String name, int frequency);

	boolean isNearby(Component component);

}
