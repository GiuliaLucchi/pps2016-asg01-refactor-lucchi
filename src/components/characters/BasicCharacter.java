package components.characters;

import java.awt.Image;

import components.Component;
import components.GameComponent;
import components.objects.GameObject;
import utils.Res;
import utils.Utils;

public class BasicCharacter extends GameComponent implements Character {

    private static final int PROXIMITY_MARGIN = 10;
    private static final int HIT_MARGIN = 5;

    protected boolean moving;
    protected boolean toRight;
    protected int counter;
    protected boolean alive;

    public BasicCharacter(int x, int y, int width, int height) {
        super(x, y, width, height);
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }

    public int getCounter() {
        return counter;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isMoving() {
        return moving;
    }

    public boolean isToRight() {
        return toRight;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Image walk(String name, int frequency) {
        String stringWalk = Res.IMG_BASE + name + (!this.moving || ++this.counter % frequency == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (this.toRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(stringWalk);
    }

    public boolean isNearby(Component component) {
        return  ((this.getX() > component.getX() - PROXIMITY_MARGIN && this.getX() < component.getX() + component.getWidth() + PROXIMITY_MARGIN)
                || (this.getX() + this.getWidth() > component.getX() - PROXIMITY_MARGIN && this.getX() + this.getWidth() < component.getX() + component.getWidth() + PROXIMITY_MARGIN));
    }

    protected boolean hitAhead (Component component) {
        if (component instanceof Character && !this.isToRight()) {
            return false;
        }
        return !(this.getX() + this.getWidth() < component.getX() || this.getX() + this.getWidth() > component.getX() + HIT_MARGIN ||
                this.getY() + this.getHeight() <= component.getY() || this.getY() >= component.getY() + component.getHeight());
    }


    protected boolean hitBack(Component component){
        return !(this.getX() > component.getX() + component.getWidth() || this.getX() + this.getWidth() < component.getX() + component.getWidth() - HIT_MARGIN ||
                this.getY() + this.getHeight() <= component.getY() || this.getY() >= component.getY() + component.getHeight());
    }

    protected boolean hitBelow(Component component) {
        int margin = 0;
        if(component instanceof GameObject) {
            margin = HIT_MARGIN;
        }
        return !(this.getX() + this.getWidth() < component.getX() + margin || this.getX() > component.getX() + component.getWidth() - margin ||
                   this.getY() + this.getHeight() < component.getY() || this.getY() + this.getHeight() > component.getY() + margin);
    }

    protected boolean hitAbove(Component component) {
        return !(this.getX() + this.getWidth() < component.getX() + HIT_MARGIN || this.getX() > component.getX() + component.getWidth() - HIT_MARGIN ||
                this.getY() < component.getY() + component.getHeight() || this.getY() > component.getY() + component.getHeight() + HIT_MARGIN);
    }
}
