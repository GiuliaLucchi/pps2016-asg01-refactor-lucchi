package components.characters;

import java.awt.Image;

import utils.Res;
import utils.Utils;

public class Mushroom extends AbstractEnemy {

    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;

    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.image = Utils.getImage(Res.IMG_MUSHROOM_DEFAULT);
    }

    protected Image getDeadImage() {
        return Utils.getImage(this.isToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }
}
