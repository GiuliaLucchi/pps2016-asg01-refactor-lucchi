package components.characters;

import java.awt.Image;

import utils.Res;
import utils.Utils;

public class Turtle extends AbstractEnemy {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;

    public Turtle(int X, int Y) {
        super(X, Y, WIDTH, HEIGHT);
        this.image = Utils.getImage(Res.IMG_TURTLE_IDLE);
    }

    protected Image getDeadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }
}
