package components.characters;

import components.Component;

import java.awt.*;

/**
 * Created by Giulia on 13/03/2017.
 */
public interface Enemy extends Character{

    Image getImage();

    void moveEnemy();

    void contact(Component component);

    Image deadImage();

}
