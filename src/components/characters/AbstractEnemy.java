package components.characters;

import components.Component;

import java.awt.*;

/**
 * Created by Giulia on 13/03/2017.
 */
public abstract class AbstractEnemy extends BasicCharacter implements Runnable, Enemy {

    private final int PAUSE = 15;

    protected Image image;
    private int offsetX;

    public AbstractEnemy(int x, int y, int width, int height) {
        super(x, y, width, height);
        this.setToRight(true);
        this.setMoving(true);
        this.offsetX = 1;

        Thread chronoEnemy = new Thread(this);
        chronoEnemy.start();
    }

    @Override
    public void run() {
        while (true) {
            if (this.isAlive()) {
                this.moveEnemy();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public void moveEnemy() {
        this.offsetX = isToRight() ? 1 : -1;
        this.setX(this.getX() + this.offsetX);

    }

    @Override
    public void contact(Component component) {
        if (this.hitAhead(component) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(component) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    @Override
    public Image deadImage(){
        return this.getDeadImage();
    }

    protected abstract Image getDeadImage();
}

