package components.characters;

import java.awt.Image;

import components.Component;
import components.objects.Object;
import game.EnemyScoreStrategy;
import game.Main;
import utils.Res;
import utils.Utils;

public class Mario extends BasicCharacter {

    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;
    private static final int JUMPING_LIMIT = 42;

    private Image imageMario;
    private boolean jumping;
    private int jumpingExtent;

    public Mario(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.imageMario = Utils.getImage(Res.IMG_MARIO_DEFAULT);
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping (boolean jumping) {
        this.jumping = jumping;
    }

    public Image doJump() {
        String stringJump;

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > Main.scene.getHeightLimit()) {
                this.setY(this.getY() - 4);
            } else {
                this.jumpingExtent = JUMPING_LIMIT;
            }

            stringJump = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if (this.getY() + this.getHeight() < Main.scene.getFloorOffsetY()) {
            this.setY(this.getY() + 1);
            stringJump = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            stringJump = this.isToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return Utils.getImage(stringJump);
    }

    public boolean contactMoney(Object money) {
        return  (this.hitBack(money) || this.hitAbove(money) || this.hitAhead(money)
                || this.hitBelow(money));
    }

    public void contactWithObject(Component object) {
        if (this.hitAhead(object) && this.isToRight() || this.hitBack(object) && !this.isToRight()) {
            Main.scene.setMovement(0);
            this.setMoving(false);
        }

        if (this.hitBelow(object) && this.jumping) {
            Main.scene.setFloorOffsetY(object.getY());
        } else if (!this.hitBelow(object)) {
            Main.scene.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (hitAbove(object)) {
                Main.scene.setHeightLimit(object.getY() + object.getHeight()); // the new sky goes below the object
            } else if (!this.hitAbove(object) && !this.jumping) {
                Main.scene.setHeightLimit(0); // initial sky
            }
        }
    }

    public void contactWithCharacter(Component character) {
        if (this.hitAhead(character) || this.hitBack(character)) {
            if (((Character) character).isAlive()) {
                this.setMoving(false);
                this.setAlive(false);
            } else {
                this.setAlive(true);
            }
        } else if (this.hitBelow(character)) {
            ((Character) character).setMoving(false);
            ((Character) character).setAlive(false);
            Main.scene.updateScore(new EnemyScoreStrategy());
        }
    }
}
