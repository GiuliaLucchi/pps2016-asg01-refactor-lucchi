package components.characters;

/**
 * Created by Giulia on 14/03/2017.
 */
public interface CharacterFactory {

    Character createMario(int x, int y);

    Enemy createMushroom(int x, int y);

    Enemy createTurtle (int x, int y);

}
