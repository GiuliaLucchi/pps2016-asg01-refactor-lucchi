package components.characters;

/**
 * Created by Giulia on 14/03/2017.
 */
public class CharacterFactoryImpl implements CharacterFactory{
    @Override
    public Character createMario(int x, int y) {
        return new Mario(x, y);
    }

    @Override
    public Enemy createMushroom(int x, int y) {
        return new Mushroom(x, y);
    }

    @Override
    public Enemy createTurtle(int x, int y) {
        return new Turtle(x, y);
    }
}
