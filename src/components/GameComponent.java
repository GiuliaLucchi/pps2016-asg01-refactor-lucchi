package components;

import game.Main;

/**
 * Created by Giulia on 12/03/2017.
 */
public class GameComponent implements Component {

    protected int width;
    protected int height;
    protected int x;
    protected int y;

    public GameComponent(int x, int y, int width, int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height= height;
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public void moveScene() {
        if (Main.scene.getxPosition() >= 0) {
            this.x = this.x - Main.scene.getMovement();
        }
    }
}
