package components.objects;

/**
 * Created by Giulia on 14/03/2017.
 */
public class ObjectFactoryImpl implements ObjectFactory {
    @Override
    public Object createBlock(int x, int y) {
        return new Block(x, y);
    }

    @Override
    public Object createMoney(int x, int y) {
        return new Money(x, y);
    }

    @Override
    public Object createTunnel(int x, int y) {
        return new Tunnel(x, y);
    }
}
