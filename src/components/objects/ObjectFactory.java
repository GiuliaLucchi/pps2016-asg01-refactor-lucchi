package components.objects;

/**
 * Created by Giulia on 14/03/2017.
 */
public interface ObjectFactory {

    Object createBlock(int x, int y);

    Object createMoney(int x, int y);

    Object createTunnel(int x, int y);

}
