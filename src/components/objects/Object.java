package components.objects;

import components.Component;

import java.awt.Image;

/**
 * Created by Giulia on 12/03/2017.
 */
public interface Object extends Component{

    Image getImageObject();
}
