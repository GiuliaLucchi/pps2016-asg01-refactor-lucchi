package components.objects;

import java.awt.Image;

import components.GameComponent;

public class GameObject extends GameComponent implements Object{

    protected Image imageObject;

    public GameObject(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    public Image getImageObject() {
        return imageObject;
    }

}
