package components;

/**
 * Created by Giulia on 12/03/2017.
 */
public interface Component {

    int getX();

    int getY();

    int getWidth();

    int getHeight();

    void setX(int x);

    void setY(int y);

    void setWidth(int width);

    void setHeight(int height);

    void moveScene();
}
