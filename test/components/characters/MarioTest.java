package components.characters;

import components.objects.Money;
import components.objects.ObjectFactoryImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Giulia on 16/03/2017.
 */
public class MarioTest {

    private final static int X_POSITION = 0;
    private final static int Y_POSITION = 0;
    private Mario mario;
    private Money money;
    private Enemy mushroom;

    @Before
    public void init(){
        this.mario = (Mario) new CharacterFactoryImpl().createMario(X_POSITION, Y_POSITION);
        this.money = (Money) new ObjectFactoryImpl().createMoney(X_POSITION, Y_POSITION);
        this.mushroom = new CharacterFactoryImpl().createMushroom(X_POSITION, Y_POSITION);
    }

    @Test
    public void isMarioJumping() throws Exception {
        this.mario.setJumping(true);
        assertTrue(this.mario.isJumping());
    }

    @Test
    public void isMarioCollectingAMoney() throws Exception {
        assertTrue(this.mario.contactMoney(this.money));
    }

    @Test
    public void isMarioDeadWhenInContactWithAnEnemy() throws Exception {
        assertTrue(this.mario.isAlive());
        this.mario.contactWithCharacter(mushroom);
        assertFalse(this.mario.isAlive());
    }

}